#!/usr/bin/env python
# -*- coding : utf-8 -*-
# main.py


import sys
from mydecipher import my_decipher

if __name__ == "__main__" :
    if len(sys.argv) < 3 :
        sys.exit(0)
    ciphered_text = None
    reference_text = None
    deciphered_text = None
    with open(sys.argv[1], 'rb') as ciphered_file :
         ciphered_text = ciphered_file.read()
    with open(sys.argv[2], 'rb') as deciphered_file :
         reference_text = deciphered_file.read()
    reference_text = reference_text.replace("\r\n", "\n").replace("\n", 
"")
    while deciphered_text != reference_text :
         deciphered_text = my_decipher(ciphered_text)
    print(deciphered_text)

